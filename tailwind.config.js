/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      'templates/**/*.{html,js}',
  ],
  theme: {
    fontFamily: {
      sans: ['Raleway', 'system-ui', 'sans-serif'],
    },
    extend: {
                },
  },
  plugins: [],
}
