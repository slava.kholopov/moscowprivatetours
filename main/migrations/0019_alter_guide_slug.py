# Generated by Django 4.2.4 on 2023-08-31 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_guide_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guide',
            name='slug',
            field=models.SlugField(blank=True, max_length=100),
        ),
    ]
