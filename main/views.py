from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin

from users.forms import BookingForm
from .models import Guide, Post, Tour, Itinerary


# Create your views here.
def index_view(request):
    tours_list = Tour.objects.filter(is_published=True).order_by('order')
    guides_list = Guide.objects.filter(is_published=True)
    posts_list = Post.objects.filter(is_featured=True)
    context = {
        'tours_list': tours_list,
        'guides_list': guides_list,
        'posts_list': posts_list,
    }
    return render(request, 'index.html', context)


class PostListView(ListView):
    template_name = 'blog.html'
    model = Post
    context_object_name = 'posts'


class PostView(DetailView):
    model = Post
    template_name = 'post.html'
    slug_field = 'slug'
    context_object_name = 'post'


class GuideView(DetailView):
    model = Guide
    template_name = 'guide.html'
    slug_field = 'slug'
    context_object_name = 'guide'


class TourListView(ListView):
    template_name = 'tours.html'
    model = Tour
    context_object_name = 'tours'
    ordering = ['order']


class TourDetailView(FormMixin, DetailView):
    template_name = 'tour.html'
    model = Tour
    form_class = BookingForm


    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['itinerary'] = Itinerary.objects.filter(tour=self.object.pk)

        return context_data


def check_availability(request):
    language = request.POST.get('first_name')
    return HttpResponse(language)
