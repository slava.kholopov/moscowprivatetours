from main.models import Tour, Guide


def menu(request):
    tours_list = Tour.objects.filter(is_published=True).order_by('order')
    guides_list = Guide.objects.filter(is_published=True)
    context = {
        'tours': tours_list,
        'guides_menu': guides_list,
    }
    return context
