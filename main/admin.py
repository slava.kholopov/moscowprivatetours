from django.contrib import admin
from django.utils.html import format_html

from main.models import Guide, Post, Tour, Itinerary
from users.admin import custom_admin_site
from users.models import Booking


class GuideAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name',  'is_english', 'is_spanish', 'is_portugues', 'is_italian', 'is_french', 'is_german', 'is_russian')
    search_fields = ('name',)

    def image_preview(self, obj):
        return format_html('<img src="{}" width="33" height="50" />', obj.image.url)


custom_admin_site.register(Guide, GuideAdmin)


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'created_date', 'is_featured')
    search_fields = ('title',)


custom_admin_site.register(Post, PostAdmin)


class BookingAdmin(admin.ModelAdmin):
    list_display = ('booking_id', 'tour_date_time', 'tour', 'status', 'payment_status', 'guide', 'full_name', 'created_date')
    search_fields = ('booking_id', 'full_name', 'email')
    list_filter = ['status', 'guide', 'tour']
    exclude = ['booking_id', 'created_date', 'full_name']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'guide':
            kwargs['queryset'] = Guide.objects.all()
        elif db_field.name == 'guide__language':
            selected_guide = kwargs['form'].instance.guide if kwargs['form'].instance else None
            if selected_guide:
                kwargs['queryset'] = Guide.objects.filter(id=selected_guide.id)
            else:
                kwargs['queryset'] = Guide.objects.none()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


custom_admin_site.register(Booking, BookingAdmin)


class ItineraryInline(admin.TabularInline):
    model = Itinerary
    extra = 0


class TourAdmin(admin.ModelAdmin):
    inlines = [ItineraryInline]
    list_display = ('title', 'order', 'image_preview',)
    search_fields = ('title',)
    ordering = ('order',)

    def image_preview(self, obj):
        return format_html('<img src="{}" width="44" height="50" />', obj.image.url)

    image_preview.short_description = 'Image'


custom_admin_site.register(Tour, TourAdmin)

