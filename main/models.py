

from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from text_unidecode import unidecode


# Create your models here.
class Guide(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField( max_length=100, blank=True)
    email = models.EmailField(max_length=50)
    phone = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(max_length=2000)
    image = models.ImageField(upload_to='static/images/guides/')
    is_published = models.BooleanField(default=True, verbose_name='published')
    review1_text = models.TextField(null=True, blank=True, max_length=700, verbose_name='Review 1 - Text')
    review1_author = models.CharField(null=True, blank=True,max_length=100, verbose_name='Review 1 - Author')
    review2_text = models.TextField(null=True, blank=True,max_length=700, verbose_name='Review 2 - Text')
    review2_author = models.CharField(null=True, blank=True,max_length=100, verbose_name='Review 2 - Author')
    review3_text = models.TextField(null=True, blank=True,max_length=700, verbose_name='Review 3 - Text')
    review3_author = models.CharField(null=True, blank=True,max_length=100, verbose_name='Review 3 - Author')
    is_english = models.BooleanField(default=False, verbose_name='english')
    is_spanish = models.BooleanField(default=False, verbose_name='spanish')
    is_portugues = models.BooleanField(default=False, verbose_name='portugues')
    is_italian = models.BooleanField(default=False, verbose_name='italian')
    is_french = models.BooleanField(default=False, verbose_name='french')
    is_german = models.BooleanField(default=False, verbose_name='german')
    is_russian = models.BooleanField(default=True, verbose_name='russian')


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'guide'
        verbose_name_plural = 'guides'
        ordering = ('name',)


class Post(models.Model):
    title = models.CharField(max_length=100)
    header = models.TextField(max_length=170)
    slug = models.SlugField(unique=True, max_length=100, blank=True)
    body = models.TextField(max_length=10000)
    image = models.ImageField(upload_to='static/images/blog/', verbose_name='image')
    created_date = models.DateField(default=timezone.now, verbose_name='created on')
    is_featured = models.BooleanField(default=False, verbose_name='featured?')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'post'
        verbose_name_plural = 'posts'
        ordering = ('-created_date',)


class Tour(models.Model):
    title_sub = models.TextField(max_length=400, blank=True, null=True)
    title = models.CharField(unique=True, max_length=30)
    header = models.CharField(max_length=40)
    description = models.TextField(max_length=1000, blank=True, null=True)
    slug = models.SlugField(unique=True, max_length=100)
    body = models.TextField(max_length=10000)
    image = models.ImageField(upload_to='static/images/tours/', verbose_name='image')
    order = models.SmallIntegerField()
    rate = models.IntegerField(default=0)

    feature1 = models.CharField(max_length=50)
    feature1_icon = models.CharField(max_length=20)
    feature2 = models.CharField(max_length=50)
    feature2_icon = models.CharField(max_length=20)
    feature3 = models.CharField(max_length=50)
    feature3_icon = models.CharField(max_length=20)
    feature4 = models.CharField(max_length=50)
    feature4_icon = models.CharField(max_length=20)

    is_featured = models.BooleanField(default=False, verbose_name='featured?')
    is_published = models.BooleanField(default=False, verbose_name='published?')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'tour'
        verbose_name_plural = 'tours'
        ordering = ('title',)


class Itinerary(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    title = models.CharField(max_length=700)
    content = models.TextField(max_length=1000, blank=True, null=True, default='\n<ul class="list-disc list-inside text-gray-500">\n<li></li>\n<li></li>\n<li></li>\n</ul>')

    def __str__(self):
        return self.title


