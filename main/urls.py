from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import TemplateView

from main.apps import MainConfig
from main.views import index_view, PostListView, PostView, GuideView, TourListView, TourDetailView, check_availability

app_name = MainConfig.name
urlpatterns = [
    path('', index_view, name='index'),
    path('blog/', PostListView.as_view(), name='blog'),
    path('blog/<slug:slug>', PostView.as_view(), name='post'),
    path('guides/<slug:slug>', GuideView.as_view(), name='guide'),
    path('tours/', TourListView.as_view(), name='tours'),
    path('tours/<slug:slug>', TourDetailView.as_view(), name='tour'),
    path('visa-to-russia', TemplateView.as_view(template_name='visa.html'), name='visa'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

htmx_urlpatterns = [
    path('check_availability/', check_availability, name='check-availability'),
]

urlpatterns += htmx_urlpatterns