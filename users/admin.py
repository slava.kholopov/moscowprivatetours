from django.contrib import admin
from django.contrib.admin import AdminSite
from main.models import Guide
from users.models import User

# Register your models here.
class CustomAdminSite(AdminSite):
    site_header = 'Moscow Private Tours'

custom_admin_site = CustomAdminSite(name='customadmin')
custom_admin_site.register(User)
