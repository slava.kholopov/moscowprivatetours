from django.contrib import messages
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect

from moscowprivatetours.settings import EMAIL_HOST_USER
from users.forms import ContactForm


# Create your views here.
def contacts(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            try:
                email = EmailMessage(
                    subject=f'Message from {name} to Moscow Private Tours',
                    body=message,
                    to=[EMAIL_HOST_USER],
                    reply_to=[email]

                )
                email.send(fail_silently=False)
                messages.success(request, 'OK')
                return render(request, 'contacts.html', {'form': None})

            except Exception as e:
                messages.error(request, 'Error')
                return render(request, 'contacts.html', {'form': form, 'show_button': True})

        else:
            messages.error(request, 'Error')
            return render(request, 'contacts.html', {'form': form, 'show_button': True})

    else:
        form = ContactForm()

    return render(request, 'contacts.html', {'form': form,'show_button': True})