from django.utils import timezone
from users.models import Booking


def mark_completed():
    pending_bookings = Booking.objects.filter(status='active')
    for booking in pending_bookings:
        if timezone.now() > booking.tour_date_time:
            booking.status = 'completed'
            booking.save()
