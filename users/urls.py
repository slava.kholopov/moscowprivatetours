from django.urls import path

from users.apps import UsersConfig
from users.views import contacts

app_name = UsersConfig.name
urlpatterns = [
    path('contacts', contacts, name='contacts'),

              ]