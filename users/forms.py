from django import forms
from django.forms import ModelForm

from users.models import Booking


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, label="Name")
    email = forms.EmailField(label="Email")
    message = forms.CharField(max_length=1000, widget=forms.Textarea, label="Message")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'mb-2 block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6'
            field.label_attrs = {'class': 'block text-sm font-semibold leading-6 text-gray-900'}


class BookingForm(ModelForm):
    class Meta:
        model = Booking
        exclude = ['booking_id', 'status', 'payment_status']


