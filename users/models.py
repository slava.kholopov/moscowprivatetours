from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

from main.models import Tour, Guide


# Create your models here.
class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True, verbose_name="email")
    name = models.CharField(max_length=50, verbose_name="name")

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    def __str__(self):
        return f' {self.name} - {self.email}'

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        ordering = ('name',)


class Booking(models.Model):
    booking_id = models.CharField(max_length=50, unique=True)
    first_name = models.CharField(max_length=50, verbose_name='First Name')
    last_name = models.CharField(max_length=100,  verbose_name='Last Name')
    full_name = models.CharField(max_length=150, verbose_name='Full Name')
    email = models.EmailField(max_length=150)
    phone = models.CharField(max_length=20, null=True, blank=True)
    participants = models.SmallIntegerField(null=True, blank=True)
    created_date = models.DateField(default=timezone.now, editable=False)
    tour_date_time = models.DateTimeField(default=timezone.now)
    meeting_point = models.CharField(max_length=300)
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    guide = models.ForeignKey(Guide, on_delete=models.CASCADE)
    language = models.CharField(max_length=20, default='English', choices=[('English','English'), ('Spanish','Español'), ('Italian', 'Italiano'), ('Portuguese', 'Português'), ('Russian','Русский'), ('German','Deutsch'), ('French','French')])
    status = models.CharField(max_length=10, default='Active', choices=[('active','Active'), ('canceled','Canceled'), ('completed','Completed')])
    payment_status = models.CharField(max_length=20, default='Not paid', choices=[('not','Not paid'), ('partly','Partly Paid'), ('fully','Fully Paid')])

    def __str__(self):
        return f'{self.booking_id} - {self.first_name} {self.last_name}'

    def save(self, *args, **kwargs):
        if not self.booking_id:
            current_date = timezone.now().strftime('%Y%m%d%H%M%S')
            self.booking_id = f"MPT{current_date}"
        super().save(*args, **kwargs)

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        verbose_name = 'booking'
        verbose_name_plural = 'bookings'
        ordering = ('tour_date_time',)

